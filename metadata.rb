depends          'ingredients'
maintainer       'David P. Kleinschmidt'
maintainer_email 'david@kleinschmidt.name'
license          'MIT'
description      'Installs/configures RabbitMQ'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.rdoc'))
version          '0.1.0'
