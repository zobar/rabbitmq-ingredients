#
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

include_recipe 'ingredients'

#
# Installs the RabbitMQ server.
#
package 'rabbitmq-server'

#
# Root certificate for verifying client certificates.
#
file '/etc/rabbitmq/cacert.pem' do
  not_if {rabbitmq.rabbit.ssl_options.cacert.nil?}

  content rabbitmq.rabbit.ssl_options.cacert
  group   'rabbitmq'
  mode    0644
  owner   'rabbitmq'

  notifies :reload, 'service[rabbitmq-server]'
end

#
# Server certificate for SSL connections.
#
file '/etc/rabbitmq/cert.pem' do
  not_if {rabbitmq.rabbit.ssl_options.cert.nil?}

  content rabbitmq.rabbit.ssl_options.cert
  group   'rabbitmq'
  mode    0644
  owner   'rabbitmq'

  notifies :reload, 'service[rabbitmq-server]'
end

#
# List of RabbitMQ plugins to enable.
#
template '/etc/rabbitmq/enabled_plugins' do
  group 'rabbitmq'
  mode  0644
  owner 'rabbitmq'
  source 'enabled_plugins.erb'

  notifies :reload, 'service[rabbitmq-server]'
end

#
# Server private key for SSL connections.
#
file '/etc/rabbitmq/key.pem' do
  not_if {rabbitmq.rabbit.ssl_options.key.nil?}

  content rabbitmq.rabbit.ssl_options.key
  group   'rabbitmq'
  mode    0600
  owner   'rabbitmq'

  notifies :reload, 'service[rabbitmq-server]'
end

#
# Main RabbitMQ configuration file.
#
template '/etc/rabbitmq/rabbitmq.config' do
  group     'rabbitmq'
  mode      0644
  owner     'rabbitmq'
  source    'rabbitmq.config.erb'
  variables rabbit: rabbitmq.rabbit, ssl: rabbitmq.rabbit.ssl_options

  notifies :reload, 'service[rabbitmq-server]'
end

#
# RabbitMQ ships with a default superuser login as guest/guest.
#
execute "clear_password:guest" do
  command "rabbitmqctl clear_password guest"
end

rabbitmq.users.each do |user, u|
  #
  # Create each user.
  #
  execute "add_user:#{user}" do
    not_if "rabbitmqctl list_users | grep -q '^#{user}\\s'"

    command "rabbitmqctl add_user #{user} '#{u.password}'"
  end

  if u.password.nil?
    #
    # If the user's password is nil, remove their password entirely.
    #
    execute "clear_password:#{user}" do
      command "rabbitmqctl clear_password #{user}"
    end
  end
end

rabbitmq.vhosts.each do |vhost, v|
  #
  # Create each vhost.
  #
  execute "add_vhost:#{vhost}" do
    not_if "rabbitmqctl list_vhosts | grep -Fqx #{vhost}"

    command "rabbitmqctl add_vhost #{vhost}"
  end

  v.permissions.each do |permission, p|
    #
    # Set permissions for each user that has access to the vhost.
    #
    execute "set_permissions:#{vhost}:#{permission}" do
      command "rabbitmqctl set_permissions -p #{vhost} #{permission} '#{p.configure}' '#{p.write}' '#{p.read}'"
    end
  end
end

#
# Manages the RabbitMQ server process.
#
service 'rabbitmq-server' do
  supports :condrestart    => true,
           :'force-reload' => true,
           :reload         => true,
           :restart        => true,
           :'rotate-logs'  => true,
           :start          => true,
           :status         => true,
           :stop           => true,
           :'try-restart'  => true
end
