#
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

Ingredients.for_cookbook :rabbitmq do
  data_bag_item 'services', 'rabbitmq'

  attribute :plugins, default: []

  namespace :rabbit do
    attribute :auth_mechanisms,     default: ['AMQPLAIN', 'PLAIN']
    attribute :ssl_cert_login_from, default: 'distinguished_name'
    attribute :ssl_listeners,       default: []
    attribute :tcp_listeners,       default: ['0.0.0.0:5672']

    def format_addresses(addresses)
      addresses.collect do |full_address|
        address, port = full_address.split ':', 2
        %({"#{address}", #{port}})
      end.join(', ')
    end

    namespace :ssl_options do
      attribute :fail_if_no_peer_cert, default: false
      attribute :verify,               default: 'verify_none'

      data_bag_attribute :cacert
      data_bag_attribute :cert
      data_bag_attribute :key
    end
  end

  search_collection :users, as: :user, sources: [:people, :services] do
    attribute :password
  end

  named_collection :vhosts do
    search_collection :permissions, sources: [:people, :services] do
      attribute :configure, default: '.*'
      attribute :read,      default: '.*'
      attribute :write,     default: '.*'
    end
  end
end
